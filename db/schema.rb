# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160121172030) do

  create_table "follows", force: :cascade do |t|
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "target_user_id", limit: 4
    t.integer  "source_user_id", limit: 4
  end

  add_index "follows", ["source_user_id"], name: "fk_rails_2c3332c856", using: :btree
  add_index "follows", ["target_user_id"], name: "fk_rails_4d99730967", using: :btree

  create_table "twits", force: :cascade do |t|
    t.string   "text",       limit: 255,             null: false
    t.integer  "rt",         limit: 4,   default: 0
    t.integer  "favs",       limit: 4,   default: 0
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
  end

  add_index "twits", ["user_id"], name: "index_twits_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "nickname",   limit: 255
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.string   "country",    limit: 255
    t.string   "bio",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_foreign_key "follows", "users", column: "source_user_id"
  add_foreign_key "follows", "users", column: "target_user_id"
  add_foreign_key "twits", "users"
end
