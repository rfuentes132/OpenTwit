class CreateFollows < ActiveRecord::Migration
  def change
    create_table :follows do |t|
      #t.references :target_user, index: { name: :target }, foreign_key: true, references: :users
      #t.references :source_user, index: { name: :source }, foreign_key: true, references: :users
      t.timestamps null: false
    end

    add_reference :follows, :target_user
    add_reference :follows, :source_user

    add_foreign_key :follows, :users, column: :target_user_id#, name: :target_user_fk
    add_foreign_key :follows, :users, column: :source_user_id#, name: :source_user_fk

  end
end
