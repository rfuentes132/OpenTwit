class CreateTwits < ActiveRecord::Migration
  def change
    create_table :twits do |t|
      t.string :text, null: false
      t.integer :rt, default: 0
      t.integer :favs, default: 0
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
