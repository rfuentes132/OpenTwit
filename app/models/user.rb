class User < ActiveRecord::Base
  has_many :follows, class_name: "Follow", foreign_key: "source_user_id"
  has_many :followers, class_name: "Follow", foreign_key: "target_user_id"

  has_many :is_following, through: :follows, source: :target_user
  has_many :is_followed_by, through: :followers, source: :source_user
end
